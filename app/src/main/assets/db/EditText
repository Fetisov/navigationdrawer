<html>
    <head>
        <title>EditText</title>
    </head>
    <body>
        <H1>EditText</H1>
<p>Виджет <b>EditText</b> — это текстовое поле для пользовательского ввода, которое используется, если необходимо редактирование текста. Следует заметить, что <b>EditText</b> является наследником <b>TextView</b>.</p>



<p>В Eclipse на панели инструментов текстовые поля можно найти в папке <b>Text Fields</b> под разными именами.</p>


<p>Для быстрой разработки в Eclipse текстовые поля снабдили различными свойствами и дали разные имена: Plain Text, Person Name, Password, Password (Numeric), E-mail, Phone, Postal Address, Multiline Text, Time, Date, Number, Number (Signed), NumberDecimal, AutoCompleteTextView, AutoCompleteTextView.</p>

<h4>Plain Text</h4>

<p><b>Plain Text</b> - самый простой вариант текстового поля без наворотов. При добавлении в разметку его XML-представление будет следующим:</p>

<pre>
&lt;EditText
    android:id=&quot;@+id/editText1&quot;
    android:layout_width=&quot;match_parent&quot;
    android:layout_height=&quot;wrap_content&quot;
    android:ems=&quot;10&quot; /&gt;
</pre>

<h4>Person Name</h4>

<p>При использовании элемента <b>Person Name</b> в XML добавляется атрибут <b>inputType</b>, который отвечает за вид клавиатуры (только буквы) при вводе текста.</p>

<pre>
&lt;EditText
    android:id=&quot;@+id/editText2&quot;
    android:layout_width=&quot;match_parent&quot;
    android:layout_height=&quot;wrap_content&quot;
    android:ems=&quot;10&quot;
    android:inputType=&quot;textPersonName&quot; /&gt;
</pre>

<h4>Password и Password (Numeric)</h4>

<p>При использовании <b>Password</b> в <b>inputType</b> используется значение <b>textPassword</b>. При вводе текста сначала показывается символ, который заменяется на звёздочку. Если используется элемент <b>Password (Numeric)</b>, то у атрибута <b>inputType</b> используется значение <b>numberPassword</b>. В этом случае на клавиатуре будут только цифры вместо букв. Вот и вся разница.</p>

<pre>4
&lt;EditText
    android:id=&quot;@+id/editText2&quot;
    android:layout_width=&quot;match_parent&quot;
    android:layout_height=&quot;wrap_content&quot;
    android:ems=&quot;10&quot;
    android:inputType=&quot;textPassword&quot; /&gt;
4</pre>

<h4>E-mail</h4>

<p>У элемента <b>E-mail</b> используется атрибут <b>android:inputType="textEmailAddress"</b>. В этом случае на клавиатуре появляется дополнительная клавиша с символом <b>@</b>, который обязательно используется в любом электронном адресе.</p>

<div class="alert alert-info">
Кстати, финны называют символ @ кошачьим хвостом (взято из Википедии). А в России символ называют собакой. Где справедливость?
</div>

<h4>Phone</h4>

<p>У элемента <b>Phone</b> используется атрибут <b>android:inputType="phone"</b>. Клавиатура похожа на клавиатуру из старого кнопочного сотового телефона с цифрами, а также с кнопками звёздочки и решётки.</p>

<h4 >Postal Address</h4>

<p>Атрибут <b>android:inputType="textPostalAddress"</b>.</p>

<h4>Multiline Text</h4>

<p>У <b>Multiline Text</b> используется атрибут <b>android:inputType="textMultiLine"</b> позволяющий сделать текстовое поле многострочным. Дополнительно можете установить свойство <b>Lines</b> (атрибут <b>android:lines</b>), чтобы указать количество видимых строк на экране.</p>

<pre>
&lt;EditText
    android:id=&quot;@+id/editText1&quot;
    android:layout_width=&quot;wrap_content&quot;
    android:layout_height=&quot;wrap_content&quot;
    android:inputType=&quot;textMultiLine&quot;
    android:lines=&quot;5&quot; &gt;
</pre>

<h4>Time и Date</h4>

<p>Атрибут <b>android:inputType="time"</b> или <b>android:inputType="date"</b>. На клавиатуре цифры, точка, запятая, тире.</p>

<h4 >Number, Number (Signed), Number (Decimal)</h4>

<p>Атрибут <b>android:inputType="number"</b> или <b>numberSigned</b> или <b>numberDecimal</b>. На клавиатуре только цифры и некоторые другие символы.</p>

<h4 >Текст-подсказка</h4>

<p>Веб-мастера знают о таком атрибуте HTML5 как placeholder, когда в текстовом поле выводится строчка-подсказка приглушенным (обычно серым цветом). Живой пример приведен ниже.</p>

<input type="text" placeholder="Введите имя кота">

<p>Подсказка видна, если текстовый элемент не содержит пользовательского текста. Как только пользователь начинает вводить текст, то подсказка исчезает. Соответственно, если удалить пользовательский текст, то подсказка появляется снова. Это очень удобное решение во многих случаях, когда на экране мало места для элементов.</p>

<p>В Android у многих элементов есть свойство <b>Hint</b> (атрибут <b>hint</b>), который работает аналогичным образом. Установите у данного свойства нужный текст и у вас появится текстовое поле с подсказкой.</p>



<p>Запускаем приложение и видим подсказку, которая исчезает при попытке ввести текст.</p>

<h4>Вызов нужной клавиатуры</h4>

<p>Не во всех случаях нужна стандартная клавиатура с буковками и цифрами. Если вы пишете калькулятор, то проще показать пользователю цифровую клавиатуру. А если нужно ввести электронный адрес, то удобнее показать клавиатуру, где уже есть символ @. Ну а если ваше приложение пишется для котов, то достаточно вывести только те буквы, из которых можно составить слова <b>Мяу</b> и <b>Жрать давай</b> (к сожалению, такой клавиатуры еще нет, но Google работает в этом направлении).</p>

<p>У элемента <b>EditText</b> на этот случай есть атрибут <b>inputType</b>:</p>

<pre>
&lt;EditText android:id=&quot;@+id/etWidth1&quot;
    android:hint=&quot;@string/catname&quot;
    <b>android:inputType=&quot;textCapWords&quot;</b>
    android:layout_height=&quot;wrap_content&quot;
    android:layout_width=&quot;wrap_content&quot;&gt;
&lt;/EditText&gt;
</pre>

<p>В данном случае с атрибутом <b>inputType=&quot;textCapWords&quot</b> каждый первый символ предложения автоматически будет преобразовываться в прописную. Удобно, не так ли?</p>

<p>Если вам нужен режим CapsLock, то используйте значение <b>textCapCharacters</b> и все буквы сразу будут большими при наборе.</p>

<p>Для набора телефонного номера используйте <b>phone</b>, и тогда вам будут доступны только цифры, звездочка (*), решетка (#).</p>

<p>Для ввода веб-адресов удобно использовать значение <b>textUri</b>. В этом случае у вас появится дополнительная кнопочка <b>.com</b> (при долгом нажатии на нее появятся альтернативные варианты .net, .org и др.).</p>

<p>Вот вам целый список доступных значений (иногда различия очень трудно различимы)</p>

<pre>
text
textCapCharacters (клавиатура с символами в верхнем регистре)
textCapWords
textCapSentences
textAutoCorrect
textAutoComplete
textMultiLine
textImeMultiLine
textNoSuggestions (без подсказок при вводе текста)
textUri
textEmailAddress
textEmailSubject
textShortMessage
textLongMessage
textPersonName
textPostalAddress
textPassword
textVisiblePassword (без автокоррекции)
textWebEditText
textFilter
textPhonetic
number
numberSigned
numberDecimal
phone
datetime
date
time
</pre>

<h4>Заблокировать текстовое поле</h4>

<p>Для блокировки текстового поля присвойте значения <i>false</i> свойствам Focusable, Long clickable и Cursor visible.</p>

<h4>Другие свойства</h4>

<dl>
<dt><b>minLines</b> и <b>maxLines</b></dt><dd>Позволяют ограничить количество строк текста, которое можно ввести в текстовом поле</dd>
<dt><b>maxLength</b></dt><dd>Позволяет задать максимальное количество символов для ввода</dd>
</dl>

<h4>Методы</h4>

<p>Основной метод класса EditText — <b>getText()</b>, который возвращает текст, содержащийся в окне элемента EditText. Возвращаемое значение имеет специальный тип Editable, а не String.</p>

<pre>
String strCatName = nickNameEditText.getText().toString(); // приводим к типу String
</pre>

<p>Соответственно, для установки текста используется метод <b>setText()</b>.</p>

<p>Большинство методов для работы с текстом унаследованы от базового класса TextView: <b>setTypeface(null, Typeface)</b>, <b>setTextSize(int textSize)</b>, <b>SetTextColor(int Color)</b>.</p>

<h4>Выделение текста</h4>

<p>У <b>EditText</b> есть специальные методы для выделения текста:</p>

<ul>
<li><b>selectAll()</b> — выделяет весь текст;</li>
<li><b>setSelection(int start, int stop)</b> — выделяет участок текста с позиции start до позиции stop;</dd>
<li><b>setSelection(int index)</b> — перемещает курсор на позицию index;</li>
</ul>

<p>Предположим, нам нужно выделить популярное слово из трёх букв в большом слове (это слово "кот", а вы что подумали?).</p>

<pre>
// выделяем 4, 5, 6 символы
edit.setSelection(3, 6);
</pre>


<h4>Обработка нажатий клавиш</h4>

<p>Для обработки нажатий клавиш необходимо зарегистрировать обработчик <b>View.OnKeyListener</b>, используя метод <b>setOnKeyListener()</b> элемента EditText. Например, для прослушивания события нажатия клавиши Enter во время ввода текста пользователем (или котом), используйте следующий код:</p>

<pre>
final EditText myText = (EditText)findViewById(R.id.EditText1);
myText.setOnKeyListener(new View.OnKeyListener()
{
    public boolean onKey(View v, int keyCode, KeyEvent event)
	{
	    if(event.getAction() == KeyEvent.ACTION_DOWN &&
		    (keyCode == KeyEvent.KEYCODE_ENTER))
			{
			    // сохраняем текст, введенный до нажатия Enter в переменную
			    String strCatName = myText.getText.getText().toString();
				return true;
			}
		return false;
	}
}
);
</pre>

<h4>Пустой ли EditText</h4>

<p>Чтобы проверить, пустой ли EditText, можно воспользоваться кодом:</p>

<pre>
if (editText.getText().toString().equals(""))
{
// Здесь код, если EditText пуст
}
else
{
// если есть текст, то здесь другой код
}
</pre>

<p>Также можно проверять длину текста, если она равно 0, значит текст пуст.</p>

<pre>
if (editText.getText().length() == 0)
</pre>

<h4>Превращаем EditText в TextView</h4>
</body>
</html>


       