package com.fetisov.navdraw;
import android.app.Application;

import com.fetisov.navdraw.utils.AssetsFindFiles;
import com.fetisov.navdraw.utils.ParseXML;
/**
 * Created by Кирилл on 26.03.2015.
 */
public class App extends Application{
    private int filesCount;
    private String[] filesPath;
    private String[] filesTitle;
    private boolean empty;

    public void onCreate() {
        super.onCreate();
        //создаем обьект для работы с активамив папке db
        AssetsFindFiles assetsFindFiles = new AssetsFindFiles(getApplicationContext().getAssets(), "db");
        filesCount = assetsFindFiles.getFilesCount();
        filesTitle = new String[filesCount];
        filesPath = new String[filesCount];
        if (filesCount == 0) {
            empty = true;
            return;
        } else {
            empty = false;
        }
        for (int i = 0; i < filesCount; i++) {
            //парсим документ и берем оттуда значение <title>, заносим в массив заголовков
            filesTitle[i] = ParseXML.getXMLTagValue(
                    assetsFindFiles.getFileInputStream(i),
                    "title"
            );
            //получаем путь к файлу из обьекта assetsfindfiles и добавляем в массив
            filesPath[i] = assetsFindFiles.getFileWebPath(i);
        }
    }

    public String[] getFilesTitle() {
        if (empty) {
            return new String[]{};
        }
        return filesTitle;
    }

    public String getFilePath(int number) {
        if (empty) {
            return null;
        }
        return filesPath[number];
    }

    public boolean isEmpty() {
        return empty;
    }
}
