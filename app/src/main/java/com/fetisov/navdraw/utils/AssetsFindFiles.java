package com.fetisov.navdraw.utils;

import android.content.res.AssetManager;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Кирилл on 26.03.2015.
 */
public class AssetsFindFiles {
    //путь к активам
    private final static String WEB_PATH = "file:///android_asset";

    private AssetManager asset;
    private String[] filesName;
    private String dir;
//конструктор устанавливает имя дирректории и путь к папке с активами
    public AssetsFindFiles(AssetManager asset, String dir) {
        this.asset = asset;
        this.dir = dir;
        try {
            //получаем массив имен файлов
            filesName = asset.list(dir);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int getFilesCount() {
        return filesName.length;
    }

    public String[] getFilesName() {
        return filesName;
    }

    public String getFileWebPath(int number) {
        if (number > filesName.length) {
            return null;
        }
        //возвращаем путь к файлу в отформатированом виде
        return String.format(
                "%s/%s/%s",
                WEB_PATH,
                dir,
                filesName[number]
        );
    }

    public InputStream getFileInputStream(int number) {
        try {
            //возвращаем дескриптор файла с номером number
            return asset.open(String.format(
                    "%s/%s",
                    dir,
                    filesName[number]
            ));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
