package com.fetisov.navdraw.utils;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Кирилл on 26.03.2015.
 */
public class ParseXML {
    public static String getXMLTagValue(final InputStream istr, final String tag) {
        XmlPullParserFactory pullParserFactory;
        try {
            pullParserFactory = XmlPullParserFactory.newInstance();
            XmlPullParser parser = pullParserFactory.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(istr, null);
            String tagName = null;
            while (parser.getEventType() != XmlPullParser.END_DOCUMENT) {
                switch (parser.getEventType()) {
                    case XmlPullParser.START_TAG:
                        tagName = parser.getName();
                        break;
                    case XmlPullParser.TEXT:
                        if (tag.equals(tagName)) {
                            return parser.getText();
                        }
                        break;
                    default:
                        break;
                }

                parser.next();
            }
        } catch (XmlPullParserException | IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
